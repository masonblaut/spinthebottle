package com.example.spinthebottle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button b_spin;
    ImageView iv_bottle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_spin = (Button) findViewById(R.id.b_spin);
        iv_bottle = (ImageView) findViewById(R.id.iv_bottle);

        b_spin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int spinDegrees;

                Random r = new Random();

                spinDegrees = r.nextInt(3600);

                RotateAnimation rotateBottle = new RotateAnimation(0, spinDegrees,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                rotateBottle.setDuration(2000);
                rotateBottle.setFillAfter(true);

                rotateBottle.setInterpolator( new AccelerateDecelerateInterpolator());

                rotateBottle.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (spinDegrees % 360 >= 270 ){
                            //person on the left has been chosen
                            Toast.makeText(MainActivity.this, "North West "+spinDegrees%360+" Degrees", Toast.LENGTH_SHORT).show();
                        }
                        else if ( 270 > spinDegrees%360 && spinDegrees%360 >= 180){
                            Toast.makeText(MainActivity.this, "South West "+spinDegrees%360+" Degrees", Toast.LENGTH_SHORT).show();
                        }
                        else if ( 180 > spinDegrees%360 && spinDegrees%360 >= 90){
                            Toast.makeText(MainActivity.this, "South East "+spinDegrees%360+" Degrees", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            //person on the right has been chosen
                            Toast.makeText(MainActivity.this, "North East "+spinDegrees%360+" Degrees", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                iv_bottle.startAnimation(rotateBottle);


            }
        });

    }
}
